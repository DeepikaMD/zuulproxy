FROM openjdk:8
MAINTAINER Deepika Muralidharan
WORKDIR /ZuulServer
COPY . /ZuulServer
CMD java -jar zuul-0.0.1-SNAPSHOT.jar
EXPOSE 8080